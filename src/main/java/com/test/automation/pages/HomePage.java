package com.test.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	WebDriver driver ;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement isHomePageAvailabile() throws Exception {
		WebElement ele = driver.findElement(By.className("login"));
		return ele;
	}
	
	public AuthenticationPage navigateToAccountPage() throws Exception{
		AuthenticationPage accountPage = new AuthenticationPage(driver);
		driver.findElement(By.className("login")).click();
		return accountPage;
		
	}
	
	
}
