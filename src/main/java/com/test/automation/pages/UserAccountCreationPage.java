package com.test.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class UserAccountCreationPage {

	WebDriver driver;

	public UserAccountCreationPage(WebDriver driver) throws Exception {
		this.driver = driver;
	}

	public WebElement isPageAvailbile() throws Exception {
		WebElement ele = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/h1"));
		return ele;
	}

	public void selectTitle(String title) throws Exception {

		switch (title) {
		case "Mrs":
			driver.findElement(By.id("id_gender2")).click();
			break;
		default:
			driver.findElement(By.id("id_gender1")).click();
			break;
		}

	}

	public void enterFirstName(String name) throws Exception {
		driver.findElement(By.id("customer_firstname")).sendKeys(name);
	}

	public void enterLastName(String name) throws Exception {
		driver.findElement(By.id("customer_lastname")).sendKeys(name);
	}

	public WebElement isEmailSelected() throws Exception {
		WebElement ele = driver.findElement(By.id("email"));
		return ele;
	}

	public void enterTheNewPassword(String password) throws Exception {
		driver.findElement(By.id("passwd")).sendKeys(password);
	}

	public void setDateOfBirth(String dob) throws Exception {
		String[] date = dob.split("-");
		Select dropdownDays = new Select(driver.findElement(By.id("days")));
		Select dropdownMonths = new Select(driver.findElement(By.id("months")));
		Select dropdownYear = new Select(driver.findElement(By.id("years")));
		dropdownDays.selectByValue(date[0]);
		dropdownMonths.selectByValue(date[1]);
		dropdownYear.selectByValue(date[2]);

	}

	public void setCustomerAddressFirstName(String firstName) throws Exception {
		driver.findElement(By.xpath("//*[@id='firstname']")).sendKeys(firstName);
	}

	public void setCustomerAddressLastName(String lastName) throws Exception {
		driver.findElement(By.xpath("//*[@id='lastname']")).sendKeys(lastName);
	}

	public void setCustomerCompanyName(String company) throws Exception {
		driver.findElement(By.xpath("//*[@id='company']")).sendKeys(company);
	}

	public void setCustomerAddressLineOne(String address1) throws Exception {
		driver.findElement(By.xpath("//*[@id='address1']")).sendKeys(address1);
	}

	public void setCustomerAddressLineTwo(String address2) throws Exception {
		driver.findElement(By.xpath("//*[@id='address2']")).sendKeys(address2);
	}

	public void setCustomerCity(String city) throws Exception {
		driver.findElement(By.xpath("//*[@id='city']")).sendKeys(city);
	}

	public void setState(String state) throws Exception {
		Select dropdownState = new Select(driver.findElement(By.id("id_state")));
		dropdownState.selectByVisibleText(state);

	}

	public void setZipCode(String zipcode) throws Exception {
		driver.findElement(By.id("postcode")).sendKeys(zipcode);
	}
	
	public void enterMobilePhoneNumber(String number) throws Exception{
		driver.findElement(By.id("phone_mobile")).sendKeys(number);
	}
	
	//TO:DO Want to implement other filling data to  Homephone , Assign an address alias for future reference. feilds, 
	
	public WebElement clickTheRegisterButton(){
		driver.findElement(By.id("submitAccount")).submit();
		WebElement ele = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div")); 
		return ele;
	}
	
	
}
